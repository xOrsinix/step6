import kotlinx.coroutines.*
import java.time.*

suspend fun returnA():Int{
    delay(400L)
    return 5
}

suspend fun returnB():Int{
    delay(400L)
    return 8
}

suspend fun main()= coroutineScope{
    val clock:Clock=Clock.systemDefaultZone()
    var t1=clock.millis()
    println(returnA()+returnB())
    var t2= clock.millis()
    println("Time = ${t2-t1}")
    t1= clock.millis()
    val num1Def=async{ returnA() }
    val num2Def=async{ returnB() }
    println(num1Def.await()+num2Def.await())
    t2= clock.millis()
    println("Time = ${t2-t1}")
}