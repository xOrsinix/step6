import java.util.HashMap

fun <T1,T2> buildMutableMap(build: HashMap<T1,T2>.()->Unit):Map<T1,T2>{
    val mapBuilder=HashMap<T1,T2>()
    mapBuilder.build()
    return mapBuilder
}

fun usage(): Map<Int, String> {
    return buildMutableMap {
        put(0, "0")
        for (i in 1..10) {
            put(i, "$i")
        }
    }
}